import React, {useState} from 'react';
import {StyleSheet, Text, TextInput, View, Button, Image, SafeAreaView, ScrollView} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import RadioForm from 'react-native-simple-radio-button';
import { Platform } from 'react-native';

export default function App() {
  const [weight, setWeight] = useState('');
  const [bottleAmount, setBottleAmount] = useState(1);
  const [timeGone, setTimeGone] = useState(1);
  const [gender, setGender] = useState('male');
  const [promille, setPromille] = useState(0);

  const bottles = [{label: '1 bottle', value: 1}];
  const time = [{label: '1 hour', value: 1}];

  for (let i = 2; i <= 24; i++ ) {
    bottles.push({label: i + ' bottles', value: i});
    time.push({label: i + ' hours', value: i});
  }

  const genders = [
    {label: 'male', value: 'male'},
    {label: 'female', value: 'female'}
  ];

  function calculate() {
    let litres = bottleAmount * 0.33;
    let grams = litres * 8 * 4.5;
    let burning = weight / 10;
    grams = grams - burning * timeGone;
    let result = 0;

    if (gender == 'male') {
      result = grams / (weight * 0.7);
    }
    else {
      result = grams / (weight * 0.6);
    }

    if (result < 0 || weight == 0) {
      result = 0;
    }

    setPromille(result);
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scroll}>
        <View style={styles.form}>
          <Text style={{fontSize: 28, padding: 10, fontWeight: 'bold', textAlign: 'center', marginBottom: 25}}><Image resizeMode={'cover'} style={{width: 56, height: 56 }} source={require("./assets/beer.png")}/>  Alcometer  <Image resizeMode={'cover'} style={{width: 56, height: 56 }} source={require("./assets/beer.png")}/></Text>
        </View>
        <View style={styles.form}>
          <View style={styles.field}>
            <Text style={{marginBottom: 3}}>Weight</Text>
            <TextInput
              style={{backgroundColor: '#fafafa', padding: 5}}
              keyboardType="number-pad"
              placeholder="kilograms"
              value={weight}
              onChangeText={text => setWeight(text)}
            >
            </TextInput>
          </View>
          <View style={[styles.field, (Platform.OS !== 'android' && {
            zIndex: 5000
            })]}>
            <Text style={{marginBottom: 3}}>Bottles</Text>
            <DropDownPicker
                zIndex={5000}
                items={bottles}
                defaultValue={bottleAmount}
                containerStyle={{height: 40}}
                itemStyle={{
                    justifyContent: 'flex-start',
                }}
                style={{
                  backgroundColor: '#fafafa',
                }}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: '#000'
                }}
                baseColor={'black'}
                onChangeItem={item => setBottleAmount(item.value)}
            />
          </View>
          <View style={[styles.field, (Platform.OS !== 'android' && {
            zIndex: 4000
            })]}>
            <Text style={{marginBottom: 3}}>Time</Text>
            <DropDownPicker
                zIndex={4000}
                items={time}
                defaultValue={timeGone}
                containerStyle={{height: 40}}
                itemStyle={{
                    justifyContent: 'flex-start'
                }}
                style={{
                  backgroundColor: '#fafafa',
                }}
                labelStyle={{
                  fontSize: 14,
                  textAlign: 'left',
                  color: '#000'
                }}
                baseColor={'black'}
                onChangeItem={item => setTimeGone(item.value)}
            />
          </View>
          <View style={[styles.field, {zIndex: -1}]}>
              <Text>Gender</Text>
              <RadioForm
                style={styles.radio}
                buttonSize = {10}
                radio_props={genders}
                initial={0}
                onPress={(value) => {setGender(value)}}
              />
          </View>
          <View style={[styles.field, {zIndex: -1}]}>
            <Button onPress={calculate} title={'Calculate'} color={'brown'}></Button>
          </View>
        </View>
        <View style={[styles.form, {zIndex:-1}]}>
          <View style={styles.field}>
            <Text style={{marginBottom: 3}}>Promilles</Text>
            <Text style={{backgroundColor: '#fafafa', padding: 5}}>{promille.toFixed(2)}</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 25,
    backgroundColor: 'white'
  },
  scroll: {
    backgroundColor: '#1c5aaf'
  },
  field: {
    margin: 10,
  },
  input: {
    marginLeft: 10
  },
  radio: {
    marginTop: 10,
    marginBottom: 10
  },
  form: {
    margin: 10,
    backgroundColor: 'lightblue',
    borderLeftWidth: 3,
    borderRightWidth: 3,
    borderTopWidth: 3,
    borderBottomWidth: 3,
    borderRadius: 10
  }
});
